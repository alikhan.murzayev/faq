# FAQ for OnePortal.
**This service was created by [Alikhan Murzayev](https://t.me/weightlight) during One-Tech golang course.**
## Run app in docker

### 1. Create network :
```shell script
docker network create oneportal
```
### 2. Run Elasticsearch:
```shell script
docker run -d --name oneportal-elastic --network oneportal -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.6.1
```
### 3. Run app:
```shell script
docker run -d --name oneportal-faq --network oneportal -p 8080:8080 -e "config=config-prod.json" **IMAGE_ID**
``` 
**After that you'll have a running app in a docker container.**

## API ROUTES
##### 1. Health check:
Returns "I'm alive!"
```
Method: GET
http://localhost:8080/check 
```
##### 2. Metrics:
Returns [Prometheus](https://prometheus.io) metrics.
```
Method: GET
http://localhost:8080/metrics
```
##### 3. Save FAQ list:
Creates or saves FAQ list and returns its UUID.
```
Methods: PUT, POST
http://localhost:8080/faq-api/faq
```
##### 4. Get FAQ list by UUID:
Returns a FAQ list by provided UUID.
```
Method: GET
http://localhost:8080/faq-api/faq/{uuid}
```
##### 5. Delete FAQ list by UUID:
Deletes FAQ list by provided UUID. In case of success of error returns "deleted successfully" or error respectively.
```
Method: DELETE
http://localhost:8080/faq-api/faq/{uuid}
```
##### 6. Change FAQ list publishing status:
Publishes or unpublishes FAQ list.
```
Method: POST
http://localhost:8080/faq-api/faq/change-status
```
##### 7. Filter FAQ lists:
Returns FAQ lists according to provided filter.
```
Method: POST
http://localhost:8080/faq-api/faq/filter
```

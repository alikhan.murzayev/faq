module gitlab.com/alikhan.murzayev/faq

go 1.13

require (
	github.com/go-kit/kit v0.10.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/olivere/elastic/v7 v7.0.12
	github.com/prometheus/client_golang v1.3.0
	github.com/sirupsen/logrus v1.4.2
)

package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/alikhan.murzayev/faq/src/config"
	"gitlab.com/alikhan.murzayev/faq/src/faq"
	"gitlab.com/alikhan.murzayev/faq/src/repository/elastic"
)

func main() {

	httpPort := flag.String("http.port", ":8080", "-http.port=:8080")
	flag.Parse()

	// Init logger
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = level.NewFilter(logger, level.AllowAll())
	logger = &serializedLogger{Logger: logger}
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	err := config.GetConfigs()
	if err != nil {
		panic(err)
	}

	// Init ElasticSearch connection
	elasticClient, err := elastic.ElasticConnectionStart()
	if err != nil {
		panic(fmt.Errorf("ElasticSearch connection error: %s\n", err))
	}
	defer elasticClient.Stop()

	// Init repos
	faqCommandRepo := elastic.NewFAQCommandRepo(elasticClient)

	fieldKeys := []string{"method"}

	// Init faq service
	faqService := faq.NewService(faqCommandRepo)
	faqService = faq.NewLoggingService(log.With(logger, "component", "faq-api"), faqService)
	faqService = faq.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "faq_service",
			Name:      "request_count",
			Help:      "Number of requests received",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "faq_service",
			Name:      "request_latency_microseconds",
			Help:      "Total duration of requests in microseconds.",
		}, fieldKeys),
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "faq_service",
			Name:      "error_count",
			Help:      "Number of error requests received",
		}, fieldKeys),
		faqService,
	)

	// Init http routes
	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()
	mux.Handle("/faq-api/", faq.MakeHandler(faqService, httpLogger))

	http.Handle("/faq-api/", accessControl(mux))
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/check", healthCheck)

	errs := make(chan error, 2)

	// Init http serve
	go func() {
		_ = logger.Log("transport", "http", "port", *httpPort, "msg", "listening faq-api")
		errs <- http.ListenAndServe(*httpPort, nil)
	}()

	// Listen errors chan
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s\n", <-c)
	}()

	_ = logger.Log("terminated", <-errs)
}

///////////////////////////////////////
// Additional structures & functions //
///////////////////////////////////////

type serializedLogger struct {
	mtx sync.Mutex
	log.Logger
}

func (l *serializedLogger) Log(keyvals ...interface{}) error {
	l.mtx.Lock()
	defer l.mtx.Unlock()
	return l.Logger.Log(keyvals...)
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "I'm alive!")
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization,X-Owner,darvis-dialog-id")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

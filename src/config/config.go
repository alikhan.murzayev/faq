package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// Global variable to store all configs
var AllConfigs Configs

// Load all configs
func GetConfigs() error {

	var filePath string

	currentDir, err := os.Getwd()
	if err != nil {
		return err
	}

	if os.Getenv("config") != "" {
		filePath = currentDir + "/src/config/" + os.Getenv("config")
	} else {
		filePath = currentDir + "/src/config/config-dev.json"
	}

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &AllConfigs)
	if err != nil {
		return err
	}

	return nil
}

// Main structure for all Configs
type Configs struct {
	Elastic ElasticConfig `json:"elastic"`
}

// Structure to store configs for ElasticSearch
type ElasticConfig struct {
	ConnectionURL []string `json:"connection_url"`
}

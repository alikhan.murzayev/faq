package domain

import (
	"time"

	"github.com/google/uuid"
)

/////////////////////
// Main structures //
/////////////////////

type QuestionsList struct {
	UUID      string     `json:"uuid"`
	Name      string     `json:"name"`
	Questions []Question `json:"questions"`
	Published bool       `json:"published"`
	CreatedOn int64      `json:"created_on"`
}

type Question struct {
	Question string `json:"question"`
	Answer   string `json:"answer"`
}

////////////////////////
// Additional methods //
////////////////////////

func (ql *QuestionsList) GenerateUUID() {
	ql.UUID = uuid.New().String()
}

func (ql *QuestionsList) SetTimestamp() {
	ql.CreatedOn = time.Now().Unix()
}

////////////////////
// Repo interface //
////////////////////

type FAQCommandRepo interface {
	Store(list *QuestionsList) error
	GetById(id string) (*QuestionsList, error)
	DeleteById(id string) error
	Filter(published, unpublished, ascending bool, from, size int) (*[]QuestionsList, int, error)
}

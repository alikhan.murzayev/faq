package faq

import (
	"context"

	"github.com/go-kit/kit/endpoint"

	"gitlab.com/alikhan.murzayev/faq/src/domain"
	"gitlab.com/alikhan.murzayev/faq/src/errors"
)

///////////////////
// FAQ endpoints //
///////////////////

// Create/Update faq req & resp
type SaveFAQRequest struct {
	domain.QuestionsList
}

type SaveFAQResponse struct {
	QuestionsListId string `json:"list_uuid"`
}

// Request validation function
func (req SaveFAQRequest) Validate() error {
	for i := range req.Questions {
		if len(req.Questions[i].Question) > 100 {
			return errors.InvalidCharacter.DevMessage("length of the questions must be <= 100")
		}
	}
	return nil
}

// Create/Update faq endpoint constructor
func makeSaveFAQEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SaveFAQRequest)
		resp, err := s.SaveQuestionsList(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Get faq req & resp
type GetFAQRequest struct {
	UUID string `json:"uuid"`
}

type GetFAQResponse struct {
	domain.QuestionsList
}

// Get faq endpoint constructor
func makeGetFAQEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetFAQRequest)
		resp, err := s.GetQuestionsListById(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Delete faq req & resp
type DeleteFAQRequest struct {
	UUID string `json:"uuid"`
}

type DeleteFAQResponse struct {
	Msg string `json:"msg"`
}

// Delete faq endpoint constructor
func makeDeleteFAQEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteFAQRequest)
		resp, err := s.DeleteQuestionsList(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Change faq published status req & resp
type ChangeFAQStatusRequest struct {
	UUID      string `json:"uuid"`
	Published bool   `json:"published"`
}

// Request validation function
func (req *ChangeFAQStatusRequest) Validate() error {
	if req.UUID == "" {
		return errors.InvalidCharacter.DevMessage("faq list uuid must be provided")
	}

	return nil
}

type ChangeFAQStatusResponse struct {
	Msg string `json:"msg"`
}

// Change faq status endpoint constructor
func makeChangeFAQStatusEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ChangeFAQStatusRequest)
		resp, err := s.ChangeQuestionsListStatus(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Filter faq req & resp
type FilterFAQRequest struct {
	Published *bool `json:"published"`
	Ascending bool  `json:"ascending"`
	From      int   `json:"from"`
	Size      int   `json:"size"`
}

type FilterFAQResponse struct {
	Lists []domain.QuestionsList `json:"lists"`
	From  int                    `json:"from"`
	Size  int                    `json:"size"`
	Total int                    `json:"total"`
}

// Filter faq endpoint constructor
func makeFilterFAQEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(FilterFAQRequest)
		resp, err := s.Filter(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

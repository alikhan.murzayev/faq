package faq

import (
	"time"

	"github.com/go-kit/kit/metrics"
)

//////////////////////////////////////////////////
// Instrumenting service struct and constructor //
//////////////////////////////////////////////////

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	requestError   metrics.Counter
	Service
}

func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, counterErr metrics.Counter, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		requestError:   counterErr,
		Service:        s,
	}
}

///////////////////////////////////
// Instrumenting service methods //
///////////////////////////////////

// Create/Update faq instrumenting service method
func (s *instrumentingService) SaveQuestionsList(req *SaveFAQRequest) (_ *SaveFAQResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "SaveQuestionsList").Add(1)
		s.requestLatency.With("method", "SaveQuestionsList").Observe(time.Since(begin).Seconds())
		if err != nil {
			s.requestError.With("method", "SaveQuestionsList").Add(1)
		}
	}(time.Now())

	return s.Service.SaveQuestionsList(req)
}

// Get faq instrumenting service method
func (s *instrumentingService) GetQuestionsListById(req *GetFAQRequest) (_ *GetFAQResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "GetQuestionsListById").Add(1)
		s.requestLatency.With("method", "GetQuestionsListById").Observe(time.Since(begin).Seconds())
		if err != nil {
			s.requestError.With("method", "GetQuestionsListById").Add(1)
		}
	}(time.Now())

	return s.Service.GetQuestionsListById(req)
}

// Delete faq instrumenting service method
func (s *instrumentingService) DeleteQuestionsList(req *DeleteFAQRequest) (_ *DeleteFAQResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "DeleteQuestionsList").Add(1)
		s.requestLatency.With("method", "DeleteQuestionsList").Observe(time.Since(begin).Seconds())
		if err != nil {
			s.requestError.With("method", "DeleteQuestionsList").Add(1)
		}
	}(time.Now())

	return s.Service.DeleteQuestionsList(req)
}

// Change faq publishing status instrumenting service method
func (s *instrumentingService) ChangeQuestionsListStatus(req *ChangeFAQStatusRequest) (_ *ChangeFAQStatusResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "ChangeQuestionsListStatus").Add(1)
		s.requestLatency.With("method", "ChangeQuestionsListStatus").Observe(time.Since(begin).Seconds())
		if err != nil {
			s.requestError.With("method", "ChangeQuestionsListStatus").Add(1)
		}
	}(time.Now())

	return s.Service.ChangeQuestionsListStatus(req)
}

// Filter faq instrumenting service method
func (s *instrumentingService) Filter(req *FilterFAQRequest) (_ *FilterFAQResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "Filter").Add(1)
		s.requestLatency.With("method", "Filter").Observe(time.Since(begin).Seconds())
		if err != nil {
			s.requestError.With("method", "Filter").Add(1)
		}
	}(time.Now())

	return s.Service.Filter(req)
}

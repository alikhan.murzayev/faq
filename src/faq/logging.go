package faq

import (
	"time"

	"github.com/go-kit/kit/log"
)

/////////////////////////////////////////////
// Logging service structure & constructor //
/////////////////////////////////////////////

type loggingService struct {
	logger log.Logger
	Service
}

func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger: logger, Service: s}
}

/////////////////////
// Logging methods //
/////////////////////

// Create/Update faq logging service method
func (s *loggingService) SaveQuestionsList(req *SaveFAQRequest) (_ *SaveFAQResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "SaveQuestionsList",
				"took", time.Since(begin),
				"err", err)
		}
	}(time.Now())

	return s.Service.SaveQuestionsList(req)
}

// Get faq logging service method
func (s loggingService) GetQuestionsListById(req *GetFAQRequest) (_ *GetFAQResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "GetQuestionsListById",
				"took", time.Since(begin),
				"err", err)
		}
	}(time.Now())

	return s.Service.GetQuestionsListById(req)
}

// Delete faq logging service method
func (s *loggingService) DeleteQuestionsList(req *DeleteFAQRequest) (_ *DeleteFAQResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "DeleteQuestionsList",
				"took", time.Since(begin),
				"err", err)

		}
	}(time.Now())

	return s.Service.DeleteQuestionsList(req)
}

// Change faq publishing status logging service method
func (s *loggingService) ChangeQuestionsListStatus(req *ChangeFAQStatusRequest) (_ *ChangeFAQStatusResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "ChangeQuestionsListStatus",
				"took", time.Since(begin),
				"err", err)
		}
	}(time.Now())

	return s.Service.ChangeQuestionsListStatus(req)
}

// Filter faq logging service method
func (s *loggingService) Filter(req *FilterFAQRequest) (_ *FilterFAQResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "Filter",
				"took", time.Since(begin),
				"err", err)
		}
	}(time.Now())

	return s.Service.Filter(req)
}

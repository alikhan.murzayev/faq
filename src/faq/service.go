package faq

import (
	"gitlab.com/alikhan.murzayev/faq/src/domain"
	"gitlab.com/alikhan.murzayev/faq/src/errors"
)

////////////////////////////////////////////
// Main service interface and constructor //
////////////////////////////////////////////
type Service interface {
	SaveQuestionsList(req *SaveFAQRequest) (*SaveFAQResponse, error)
	GetQuestionsListById(req *GetFAQRequest) (*GetFAQResponse, error)
	DeleteQuestionsList(req *DeleteFAQRequest) (*DeleteFAQResponse, error)
	ChangeQuestionsListStatus(req *ChangeFAQStatusRequest) (*ChangeFAQStatusResponse, error)
	Filter(req *FilterFAQRequest) (*FilterFAQResponse, error)
}

type service struct {
	FAQCommandRepo domain.FAQCommandRepo
}

func NewService(repo domain.FAQCommandRepo) Service {
	return &service{FAQCommandRepo: repo}
}

/////////////////////////
// FAQ service methods //
/////////////////////////

// Create/Update faq service method
func (s *service) SaveQuestionsList(req *SaveFAQRequest) (*SaveFAQResponse, error) {

	list := req.QuestionsList

	if list.UUID == "" {
		list.GenerateUUID()
		list.SetTimestamp()
	}

	err := s.FAQCommandRepo.Store(&list)
	if err != nil {
		return nil, err
	}

	return &SaveFAQResponse{QuestionsListId: list.UUID}, nil
}

// Get faq service method
func (s *service) GetQuestionsListById(req *GetFAQRequest) (*GetFAQResponse, error) {

	list, err := s.FAQCommandRepo.GetById(req.UUID)
	if err != nil {
		return nil, err
	}

	return &GetFAQResponse{QuestionsList: *list}, nil
}

// Delete faq service method
func (s *service) DeleteQuestionsList(req *DeleteFAQRequest) (*DeleteFAQResponse, error) {
	err := s.FAQCommandRepo.DeleteById(req.UUID)
	if err != nil {
		return nil, err
	}
	return &DeleteFAQResponse{Msg: "deleted successfully"}, nil
}

// Change faq publishing status service method
func (s *service) ChangeQuestionsListStatus(req *ChangeFAQStatusRequest) (*ChangeFAQStatusResponse, error) {

	list, err := s.FAQCommandRepo.GetById(req.UUID)
	if err != nil {
		return nil, err
	}

	if list.Published == req.Published {
		return nil, errors.InvalidCharacter.DevMessage("status is already set")
	}

	list.Published = req.Published
	err = s.FAQCommandRepo.Store(list)
	if err != nil {
		return nil, err
	}

	return &ChangeFAQStatusResponse{Msg: "status successfully changed"}, nil
}

// Filter faq service method
func (s *service) Filter(req *FilterFAQRequest) (*FilterFAQResponse, error) {

	var published, unpublished bool

	if req.Published == nil {
		published, unpublished = true, true
	} else {
		published = *req.Published
		unpublished = !*req.Published
	}

	lists, total, err := s.FAQCommandRepo.Filter(published, unpublished, req.Ascending, req.From, req.Size)
	if err != nil {
		return nil, err
	}

	resp := FilterFAQResponse{
		Lists: *lists,
		From:  req.From,
		Size:  len(*lists),
		Total: total,
	}

	return &resp, nil

}

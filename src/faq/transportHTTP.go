package faq

import (
	"context"
	"encoding/json"
	"net/http"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	"gitlab.com/alikhan.murzayev/faq/src/errors"
)

//////////////////////////
// HTTP routes handlers //
//////////////////////////
func MakeHandler(s Service, logger kitlog.Logger) http.Handler {

	// Options array
	options := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(encodeError),
	}

	// FAQ routes handlers
	saveFAQList := kithttp.NewServer(
		makeSaveFAQEndpoint(s),
		decodeSaveFAQRequest,
		encodeResponse,
		options...)

	getFAQList := kithttp.NewServer(
		makeGetFAQEndpoint(s),
		decodeGetFAQRequest,
		encodeResponse,
		options...)

	deleteFAQ := kithttp.NewServer(
		makeDeleteFAQEndpoint(s),
		decodeDeleteFAQRequest,
		encodeResponse,
		options...)

	changeFAQStatus := kithttp.NewServer(
		makeChangeFAQStatusEndpoint(s),
		decodeChangeFAQStatusRequest,
		encodeResponse,
		options...)

	filterFAQ := kithttp.NewServer(
		makeFilterFAQEndpoint(s),
		decodeFilterFAQRequest,
		encodeResponse,
		options...)

	// Init routes
	r := mux.NewRouter()

	// FAQ routes
	r.Handle("/faq-api/faq", saveFAQList).Methods("PUT", "POST")
	r.Handle("/faq-api/faq/{uuid}", getFAQList).Methods("GET")
	r.Handle("/faq-api/faq/{uuid}", deleteFAQ).Methods("DELETE")
	r.Handle("/faq-api/faq/change-status", changeFAQStatus).Methods("POST")
	r.Handle("/faq-api/faq/filter", filterFAQ).Methods("POST")

	return r
}

//////////////////////////
// FAQ request decoders //
//////////////////////////

// Create/Update request decoder
func decodeSaveFAQRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body SaveFAQRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	_ = r.Body.Close()
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	err = body.Validate()
	if err != nil {
		return nil, err
	}

	return body, nil
}

// Get faq request decoder
func decodeGetFAQRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body GetFAQRequest
	vars := mux.Vars(r)

	id, ok := vars["uuid"]
	if !ok {
		return nil, errors.InvalidCharacter.DevMessage("faq list uuid must be provided")
	}

	body.UUID = id

	return body, nil
}

// Delete faq request decoder
func decodeDeleteFAQRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body DeleteFAQRequest
	vars := mux.Vars(r)

	id, ok := vars["uuid"]
	if !ok {
		return nil, errors.InvalidCharacter.DevMessage("faq list uuid must be provided")
	}

	body.UUID = id

	return body, nil
}

// Change faq status request decoder
func decodeChangeFAQStatusRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body ChangeFAQStatusRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	_ = r.Body.Close()
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	err = body.Validate()
	if err != nil {
		return nil, err
	}

	return body, nil
}

// Filter faq request decoder
func decodeFilterFAQRequest(_ context.Context, r *http.Request) (interface{}, error) {

	var body FilterFAQRequest

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	if body.Size == 0 {
		body.Size = 10
	}

	return body, nil
}

///////////////////////
// Response encoders //
///////////////////////
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	e, ok := response.(errorer)
	if ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

////////////////////////////
// Error response encoder //
////////////////////////////
type errorer interface {
	error() error
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if e, ok := err.(*errors.ArgError); ok {
		w.WriteHeader(e.Status)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_ = json.NewEncoder(w).Encode(err)
}

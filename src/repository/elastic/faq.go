package elastic

import (
	"context"
	"encoding/json"

	"github.com/olivere/elastic/v7"
	
	"gitlab.com/alikhan.murzayev/faq/src/domain"
	"gitlab.com/alikhan.murzayev/faq/src/errors"
)

type faqCommandRepo struct {
	client *elastic.Client
}

func (repo *faqCommandRepo) Store(list *domain.QuestionsList) error {
	_, err := repo.client.Index().
		Index("faq").
		Id(list.UUID).
		BodyJson(list).
		Refresh("true").
		Do(context.TODO())
	if err != nil {
		return errors.ElasticSaveError.DevMessage(err.Error())
	}

	return nil
}

func (repo *faqCommandRepo) GetById(id string) (*domain.QuestionsList, error) {

	var questionList domain.QuestionsList

	resp, err := repo.client.Get().Index("faq").Id(id).Pretty(true).Do(context.TODO())
	if err != nil {
		return nil, errors.ElasticConnectError.DevMessage(err.Error())
	}

	if !resp.Found {
		return nil, errors.NoContentFound
	}

	err = json.Unmarshal(resp.Source, &questionList)
	if err != nil {
		return nil, errors.DeserializeBug.DevMessage(err.Error())
	}

	return &questionList, nil
}

func (repo *faqCommandRepo) DeleteById(id string) error {
	resp, err := repo.client.Delete().Index("faq").Id(id).Pretty(true).Do(context.TODO())
	if err != nil {
		return errors.ElasticConnectError.DevMessage(err.Error())
	}

	if resp.Shards.Failed != 0 {
		return errors.NoContentFound
	}

	return nil
}

func (repo *faqCommandRepo) Filter(published, unpublished, ascending bool, from, size int) (*[]domain.QuestionsList, int, error) {

	query := elastic.NewBoolQuery()
	if published {
		termQuery := elastic.NewTermQuery("published", "true")
		query = query.Should(termQuery)
	}

	if unpublished {
		termQuery := elastic.NewTermQuery("published", "false")
		query = query.Should(termQuery)
	}

	// Count faq by query
	total, err := repo.client.Count("faq").Query(query).Do(context.TODO())
	if err != nil {
		return nil, 0, errors.ElasticConnectError.DevMessage(err.Error())
	}
	if total == 0 {
		return nil, 0, errors.NoContentFound
	}

	// Get faq by query
	resp, err := repo.client.Search("faq").
		Query(query).
		Sort("created_on", ascending).
		From(from).
		Size(size).
		Do(context.TODO())

	if err != nil {
		return nil, 0, errors.ElasticConnectError.DevMessage(err.Error())
	}

	if resp.Hits.TotalHits.Value == 0 {
		return nil, 0, errors.NoContentFound
	}

	var lists []domain.QuestionsList

	for _, hit := range resp.Hits.Hits {
		var list domain.QuestionsList
		err := json.Unmarshal(hit.Source, &list)
		if err != nil {
			return nil, 0, errors.DeserializeBug.DevMessage(err.Error())
		}

		lists = append(lists, list)
	}

	return &lists, int(total), nil

}

// repo constructor
func NewFAQCommandRepo(client *elastic.Client) domain.FAQCommandRepo {
	return &faqCommandRepo{client: client}
}

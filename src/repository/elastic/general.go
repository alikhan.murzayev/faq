package elastic

import (
	"github.com/olivere/elastic/v7"

	"gitlab.com/alikhan.murzayev/faq/src/config"
	"gitlab.com/alikhan.murzayev/faq/src/errors"
)

func ElasticConnectionStart() (*elastic.Client, error) {

	client, err := elastic.NewClient(elastic.SetURL(config.AllConfigs.Elastic.ConnectionURL...), elastic.SetSniff(true))
	if err != nil {
		return nil, errors.ElasticConnectError.DevMessage(err.Error())
	}

	return client, nil
}
